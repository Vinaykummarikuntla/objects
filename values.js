exportObjects = {
    values (obj) {
        const valuesList = [];
        for(property in obj) {
            if(!(obj[property] instanceof Function)) {
                valuesList.push(obj[property]);
            }
        }
        return valuesList;
    }
}
module.exports = {exportObjects};