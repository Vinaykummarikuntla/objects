exportObjects = {
    mapObject (obj, cb) {
        for(property in obj) {
            if(!(obj[property] instanceof Function)) {
                obj[property] = cb(obj[property]);
            }
        }
        return obj;
    }
}
module.exports = {exportObjects};