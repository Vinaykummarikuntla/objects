
exportObjects = {
    invert (obj) {
        invertedObject = {};
        for(property in obj) {
            if(obj[property] instanceof Function) {
                invertedObject[property] = obj[property];
            } else {
                invertedObject[obj[property]] = property;
            }
        }
        return invertedObject;
    }
}
module.exports = {exportObjects};