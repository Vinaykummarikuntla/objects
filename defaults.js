
exportObjects = {
    defaults (obj, defaultProps) {
        for(property in defaultProps) {
            obj[property] = defaultProps[property];
        }
        return obj;
    }
}
module.exports = {exportObjects};
