exportObjects = {
    keys (obj) {
        const keysList = [];
        for(property in obj) {
            if(!(obj[property] instanceof Function)) {
                keysList.push(property);
            }
        }
        return keysList;
    }
}
module.exports = {exportObjects};